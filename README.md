# Game-of-credit
3D first person shooter

[![Circle CI](https://circleci.com/gh/svagi/Game-of-credit.svg?style=shield&circle)](https://circleci.com/gh/svagi/Game-of-credit)
[![Dependency Status](https://david-dm.org/svagi/Game-of-credit.png)](https://david-dm.org/svagi/Game-of-credit)
[![devDependency Status](https://david-dm.org/svagi/Game-of-credit/dev-status.png)](https://david-dm.org/svagi/Game-of-credit#info=devDependencies)

# Installation
Install global CoffeeScript, Webpack & Webpack-dev-server

    npm install coffee-script -g
    npm install webpack -g
    npm install webpack-dev-server -g

Then install all dependencies

    npm install

Finally run application in development mode with

    npm start
